<h1><span style="color: #0000ff; background-color: #ccffcc;"><strong>READ-ME !&nbsp;</strong></span></h1>
<p><span style="background-color: #ccffcc;">This project is a demo Cypress project which will help users to setup a e2e cypress automation project.</span></p>
<div>
<h3><span style="background-color: #ccffcc;">Steps to Setup:</span></h3>
<h4><span style="background-color: #ccffcc;">Pre-requisite :</span></h4>
<ul>
<li><span style="background-color: #ccffcc;">NVM , NPM and Node should be install.</span></li>
</ul>
<div>
<div><span style="background-color: #ccffcc;"><strong>How ?&nbsp;</strong></span></div>
<div><span style="background-color: #ccffcc;"><strong>Refer to the documents below to install the above.</strong></span></div>
<div><span style="background-color: #ccffcc;"><a style="background-color: #ccffcc;" href="https://docs.npmjs.com/downloading-and-installing-node-js-and-npm">https://docs.npmjs.com/downloading-and-installing-node-js-and-npm</a></span></div>
<div><span style="background-color: #ccffcc;">&nbsp;</span></div>
<h4><span style="background-color: #ccffcc;"><strong>Steps installing Cypress :&nbsp;</strong></span></h4>
<div><span style="background-color: #ccffcc;">Operating System : Windows 11&nbsp;</span></div>
<div>
<ul>
<li><span style="background-color: #ccffcc;">Open command prompt</span></li>
<li><span style="background-color: #ccffcc;">Navigate to your project directory</span></li>
<li><span style="background-color: #ccffcc;">cd /your/project/path</span>
<ul>
<li><span style="background-color: #ccffcc;">e.g. C:/Projects/Deb</span></li>
</ul>
</li>
</ul>
<br />
<div><span style="color: #ff6600; background-color: #ccffcc;"><em>Note : </em></span></div>
<div><span style="color: #ff6600; background-color: #ccffcc;"><em>Before we install cypress ,we need to ensure that our project folder has package.json or node_modules&nbsp;</em></span></div>
<br />
<div><span style="background-color: #ccffcc;"><strong>Type the below mentioned command:</strong></span></div>
<ul>
<li><span style="background-color: #ccffcc;">npm init</span>
<ul>
<li><span style="background-color: #ccffcc;">( Fill up the questionaries to create package.json file ) This will create the package.json file in the project folder.</span></li>
</ul>
</li>
</ul>
<div>
<div><span style="background-color: #ccffcc;">Once package.json file is created then</span></div>
<br />
<div><span style="background-color: #ccffcc;"><strong>Type the below mentioned command</strong></span></div>
<ul>
<li><span style="background-color: #ccffcc;">npm install cypress@9.4.1 --save-dev</span></li>
</ul>
<p><span style="background-color: #ccffcc;">Or&nbsp;</span></p>
<ul>
<li><span style="background-color: #ccffcc;">npm install cypress --save-dev &nbsp; &nbsp;</span></li>
</ul>
<div><span style="color: #ff6600; background-color: #ccffcc;"><em>Note : </em></span></div>
<div><span style="color: #ff6600; background-color: #ccffcc;"><em>This will install Cypress locally as a Dev Dependencies in your project.</em></span></div>
<div><span style="background-color: #ccffcc;">&nbsp;</span></div>
<div><span style="background-color: #ccffcc;"><strong><span style="color: #ff6600;"><em><span style="color: #ff0000;">Error !!</span> </em></span></strong></span></div>
<span style="color: #ff6600; background-color: #ccffcc;"><em>Sometimes installation may also fail due to node version , so reinstall the latest version and try again.</em></span><br />
<div><span style="background-color: #ccffcc;">&nbsp;</span></div>
<div><span style="background-color: #ccffcc;">&nbsp;</span></div>
<div><span style="background-color: #ccffcc;"><strong>Now Update the Package.JSON File</strong></span></div>
<br />
<div><span style="background-color: #ccffcc;">Navigate to Package.json file inside the project folder:</span></div>
<div><span style="background-color: #ccffcc;">Change the below mentioned item in package.json</span></div>
<div><span style="background-color: #ccffcc;">&nbsp;</span></div>
<div><span style="background-color: #ccffcc;"><em><span style="color: #0000ff;">"scripts": {</span></em></span></div>
<div><span style="background-color: #ccffcc;"><em><span style="color: #0000ff;">&nbsp; &nbsp; "test": "cypress open"</span></em></span></div>
<div><span style="background-color: #ccffcc;"><em><span style="color: #0000ff;">&nbsp; &nbsp; }</span></em></span></div>
<div><span style="background-color: #ccffcc;">&nbsp;</span></div>
<div>
<h4><span style="background-color: #ccffcc;">Verify Cypress installed or not</span></h4>
<ul>
<li><span style="background-color: #ccffcc;">Open IDE</span></li>
<li><span style="background-color: #ccffcc;">Navigate to Terminal</span></li>
<li><span style="background-color: #ccffcc;">Run Command - <span style="color: #0000ff;"><em>npm run test</em></span></span></li>
</ul>
<div><span style="color: #ff6600; background-color: #ccffcc;"><em>Note - This will run the Script command that we have added in package.json section</em></span></div>
</div>
<div><span style="background-color: #ccffcc;">&nbsp;</span></div>
<div><span style="background-color: #ccffcc;">&nbsp;</span></div>
<div>&nbsp;</div>
<div><strong><span style="background-color: #ccffcc;">How to Run :&nbsp;</span></strong></div>
<div>
<ul>
<li>
<p><em>Run cypress while passing the record key</em></p>
</li>
<li>
<p><em>npx cypress run --record --key &lt;<strong>Record Number</strong>&gt;</em></p>
</li>
</ul>
<p><em>Or&nbsp;</em></p>
<p><em><strong><span style="background-color: #ccffcc;">How to Run CLI:&nbsp;</span></strong></em></p>
<ul>
<li><em>Navigate to Root Folder :&nbsp;</em></li>
<li><em>Type the below mentioned command to run all the spec files</em>
<ul>
<li><em>node_modules\.bin\cypress run</em></li>
</ul>
</li>
<li><em>If you want to run a specific Spec File </em></li>
<li><em><em>Type the below mentioned command</em></em>
<ul>
<li><em><em>node_modules\.bin\cypress run --spec cypress\integration\Specs\headerValidation.spec.js</em></em></li>
</ul>
</li>
</ul>
</div>
<h4><br />Run from Script File Package.json:&nbsp;</h4>
<div>
<div><em>"scripts": {</em></div>
<div><em>&nbsp; &nbsp; "cy:open": "cypress open",</em></div>
<div><em>&nbsp; &nbsp; "cy:test":"cypress run --browser chrome --spec 'cypress/integration/Specs/*.spec.js' --env Env=test",</em></div>
<div><em>&nbsp; &nbsp; "cy:staging":"cypress run --browser chrome --spec 'cypress/integration/Specs/*.spec.js' --env Env=staging"</em></div>
<div><em>&nbsp; },</em></div>
<div>&nbsp;</div>
<div><em>npm run cy:test - Will run script with Test Environment credentials&nbsp;</em></div>
<div>&nbsp;</div>
<div><em>npm run cy:staging - Will run script with Test Environment credentials&nbsp;</em></div>
<div>&nbsp;</div>
</div>
</div>
</div>
</div>
</div>