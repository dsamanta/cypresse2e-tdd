<!-- #######  THIS IS A COMMENT - Visible only in the source editor #########-->
<h2>Read me !&nbsp;</h2>
<p>This is read me for E2E Testing.&nbsp;</p>
<p>This is a cypress e2e automation framework repo. This automation framework has used Cypress as a front end testing tool which is a&nbsp;purely JavaScript-based testing built tool.</p>
<p><strong>Tools and Framework:&nbsp;</strong></p>
<table style="height: 15px; width: 566px;">
<tbody>
<tr>
<td style="width: 58.5px;"><strong>Tool/Frame work Used</strong></td>
<td style="width: 153.5px;">&nbsp;</td>
</tr>
<tr>
<td style="width: 58.5px;">Front Testing Built tool</td>
<td style="width: 153.5px;">Cypress</td>
</tr>
<tr>
<td style="width: 58.5px;">Language</td>
<td style="width: 153.5px;">Java Script</td>
</tr>
<tr>
<td style="width: 58.5px;">Page Model&nbsp;</td>
<td style="width: 153.5px;">POM ( Page Object Model )&nbsp;</td>
</tr>
<tr>
<td style="width: 58.5px;">Framework</td>
<td style="width: 153.5px;">BDD , Mocha&nbsp;</td>
</tr>
<tr>
<td style="width: 58.5px;">Reporting&nbsp;</td>
<td style="width: 153.5px;">Mochasome</td>
</tr>
<tr>
<td style="width: 58.5px;">containerisation Platform</td>
<td style="width: 153.5px;">Docker</td>
</tr>
</tbody>
</table>
<p style="font-size: 1.5em;"><strong>Page Object Model :&nbsp;</strong></p>
<p><strong>Spec File:</strong></p>
<p>All the test cases are maintained under Spec files.</p>
<p>Navigate to Root Folder :&nbsp;</p>
<ul>
<li>./cypress/integration/Specs&nbsp;</li>
</ul>
<p><strong>Page File:</strong></p>
<p>Page class should be created for each webpage. Page class should contain page elements and action methods to execute test case objectives.</p>
<p>Navigate to Root Folder :&nbsp;</p>
<ul>
<li>./cypress/pages</li>
</ul>
<p><strong>Test Data File:</strong></p>
<p>Test data should be maintained for each environement. In this framework testdata is maintained in a json file inside the fixture folder.</p>
<p>Navigate to Root Folder :&nbsp;</p>
<ul>
<li>./cypress/fixtures/testData.json&nbsp;</li>
</ul>
<p><strong>Configuation file:</strong></p>
<p>Configuation file should be&nbsp;maintained to document and use test automation configuration requirements. cypress.json is a test runner as well as configuration file.</p>
<p>Navigate to Root Folder :</p>
<ul>
<li>./cypress/cypress.json</li>
</ul>
<p><strong>MakeFile:&nbsp;</strong></p>
<p>Makefile is used for Orchestration of npm commands to control framework test automation scripts.</p>
<p>Navigate to Root Folder :</p>
<ul>
<li>./cypress/Makefile</li>
</ul>
<p>&nbsp;</p>
<p><strong>How to Run Test Automation Scripts:&nbsp;</strong></p>
<p>All command needs to be executed from root folder&nbsp;</p>
<p><strong>Local : </strong></p>
<p><strong>Chrome Browser/Test Environment</strong></p>
<div>
<ul>
<li>make local/chrome-test</li>
</ul>
<p><strong>EdgeBrowser/Test Environment</strong></p>
<div>
<ul>
<li>make local/edge-test</li>
</ul>
<p><strong>Electron Browser/Test Environment</strong></p>
<div>
<ul>
<li>make local/electron-test</li>
</ul>
<p><strong>Docker / Chrome Browser/Test Environment</strong></p>
<div>
<ul>
<li>make docker/chrome <em>environment=&lt;e.g test&gt;</em>
<ul>
<li>make docker/chrome <em>environment=&lt;e.g test&gt;</em></li>
</ul>
</li>
</ul>
<p>&nbsp;</p>
<p><strong><em>Reports:&nbsp;</em></strong></p>
<p><em>Reports are generated using mochasome. Reports are created merged and kept it in e2e root folder.</em></p>
<p>&nbsp;</p>
</div>
</div>
</div>
</div>