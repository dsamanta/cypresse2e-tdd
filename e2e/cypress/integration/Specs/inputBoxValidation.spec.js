/// <reference types="cypress" />
import boxPage from '../../pages/inputBoxPage';

const boxElement = new boxPage();

describe('Validate Input Boxes',function() {  
  before(function () {
      cy.fixture('testData.json').then(function (data) {
      this.data = data; 
      })
    })

  it('Validate Input box',function () {
    // Read BaseUrl of Environment as per Env details shared in CLI
     cy.visit(Cypress.env(Cypress.env().Env+"_inputboxValidationUrl")); 
     const name = Cypress.env('username');
     expect(name).to.equal('staging');

     //Validat if user id Exists and enter the value
     boxElement.enterUserId('Debashish'); 

     //Select a Particular CheckBox (In this example first one)
     boxElement.selectCheckBox(0);

     //Select a Particular Radio Button (In this example first one)
     boxElement.selectRadioButton(0);

     //Select a Particular Value from the list (In this example 3rd one)
     boxElement.selectValue(2);

     //Select a Particular Dropdown Value from the list (In this example last one)
     boxElement.selectDropdownValue('Drop Down Item 6');

     
  })
})