/// <reference types="cypress" />



export default class inputboxPage { 
    // Elements 
    inputBoxHeader(){
        return cy.get("[id='HTMLFormElements']");
    }

    inputBox(){
        return cy.get("input[name='username']");
    }

    checkBox(){
        return cy.get("input[type='checkbox']");
    }

    enterPwd(){
        return cy.get("input[name='password']");
    }

    radioButton(){
        return cy.get("input[type='radio']");
    }

    multipleValue(){
        return cy.get("select[name='multipleselect[]']").find("option");
    }

    dropdownValue(){
        return cy.get("select").eq(1);
    }
    
    //Action Methods : 
    enterUserId(Name){
        this.inputBoxHeader().find("td").eq(0).should('include.text','Username');
        this.inputBox().type(Name);
    }

    selectCheckBox(checkBoxNumber){
        this.checkBox().eq(checkBoxNumber).click();
    }

    selectRadioButton(radioButtonNumber){
        this.radioButton().eq(radioButtonNumber).click();
    }

    selectValue(selectValueNumber){
        this.multipleValue().eq(selectValueNumber).click();
    }

    selectDropdownValue(dropdownValue){
        this.dropdownValue().select(dropdownValue);
    }
    
}
